/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;
import ufps.util.colecciones_seed.*;

/**
 *
 * @author estudiante
 */
public class Expresion {
    
  private ListaCD<String> expresiones=new ListaCD();  

    public Expresion() {
    }
    
    public Expresion(String cadena) {
        
       String v[]=cadena.split(",");
       for(String dato:v) 
           this.expresiones.insertarAlFinal(dato);
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
     String msg="";
     for(String dato:this.expresiones)
         msg+=dato+"<->";
    return msg;
    }
    
    public String getPrefijo() throws Exception
    {
        if(this.expresiones.esVacia() || this.expresiones.getTamanio()<=2)
            return "la ecuacion no concuerda";
        
        ListaCD<String> lista=this.expresiones;
        ListaCD<String> lista2=new ListaCD<>();
        int con=0;
        Pila pila = new Pila();
        for(String dato:lista){
            if(esSigno(dato)){
                if(pila.esVacia()){
                    pila.apilar(dato);
                }else{
                    int pe=prioridadExpresiones(dato);
                    int ps=prioridadSigno((String) pila.getTope());
                    if(pe > ps){
                        pila.apilar(dato);
                    }else{
                        if(pe==1)
                            con=lista2.getTamanio();
                        
                        int pos=lista2.getTamanio()-con;
                        lista2.insertarNodosPos(pos,(String)pila.desapilar());
                        pila.apilar(dato);
                        con=0;
                        
                    }
                }
            }else{
                lista2.insertarAlFinal(dato);
                con++;
            }
        }
        while(!pila.esVacia()){
            if(prioridadExpresiones((String)pila.getTope())==1)
                con=lista2.getTamanio();
            
            int pos=lista2.getTamanio()-con;
            lista2.insertarNodosPos(pos,(String)pila.desapilar());
            con=lista2.getTamanio();
        }
        return convertir(lista2);
    }
    
    private String convertir(ListaCD<String> lista) {
        if(lista.esVacia() || lista.getTamanio()<=2)
            return "la ecuacion no concuerda";
        
        String pre="";
        for(String dato:lista)
            pre+=dato;
        
        return pre;
    }
    
    public String getPosfijo()
    {
        if(this.expresiones.esVacia() || this.expresiones.getTamanio()<=2)
            return "la ecuacion no concuerda";
        
        ListaCD<String>lista=this.expresiones;
        String pos="";
        Pila pila = new Pila();
        
        for(String dato:lista){
            if(esSigno(dato)){
                if(pila.esVacia()){
                    pila.apilar(dato);
                }else{
                    int pe=prioridadExpresiones(dato);
                    int ps=prioridadSigno((String) pila.getTope());
                    if(pe > ps){
                        pila.apilar(dato);
                    }else{
                        if(pila.getTope()!="(" || pila.getTope()!=")")
                            pos+=pila.desapilar();
                        
                        if(")".equals(dato)){
                            while(!pila.esVacia() && pila.getTope()!="("){
                                pos+=pila.desapilar();
                            }
                        }else if(!pila.esVacia()){
                            if(pila.getTope()=="("){
                                pila.desapilar();
                            }
                        }
                        
                        pila.apilar(dato);
                        if(!pila.esVacia()){
                            if(pila.getTope()==")"){
                                pila.desapilar();
                            }
                        }
                        
                    }
                }
            }else{
                pos+=dato;
            }
        }
        while(!pila.esVacia()){
            pos+=pila.desapilar();
        }
        return pos;
    }
    
    
    private boolean esSigno(String a){
        return "+".equals(a) || "-".equals(a) || "*".equals(a) || "/".equals(a) || "(".equals(a) || ")".equals(a);
    }
    
    private int prioridadExpresiones(String signo){
        if("/".equals(signo) || "*".equals(signo))return 2;
        if("-".equals(signo) || "+".equals(signo))return 1;
        if("(".equals(signo))return 3;
        return 0;
    }
    
    private int prioridadSigno(String signo){
        if("/".equals(signo) || "*".equals(signo))return 2;
        if("-".equals(signo) || "+".equals(signo))return 1;
        if("(".equals(signo))return 0;
        return 0;
    }
    
    public float getEvaluarPosfijo()
    {
        return 0.0f;
    }

    

    
    
}
